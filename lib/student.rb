class Student

  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{self.first_name} #{self.last_name}"
  end

  def enroll(new_course)
    raise if @courses.any? { |course| course.conflicts_with?(new_course) }
    self.courses << new_course unless self.courses.include?(new_course)
    new_course.students << self unless new_course.students.include?(self)
  end

  def course_load
    credits_load = Hash.new(0)
    self.courses.each do |course|
      credits_load[course.department] += course.credits
    end
    credits_load
  end
end
